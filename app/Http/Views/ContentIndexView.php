<?php

namespace App\Http\Views;

use App\Model\MultiColumnVariable;
use App\Model\MultiVariableContent;

class ContentIndexView extends DefaultView
{
	// public function __construct ($)
	// {
	// 	// code...
	// }
	/**
	 * Return array data
	 * @return array
	 */
	public function toArray() : array
	{
		return [
				'slider' => $this->getSliderArray(),
				'gallery' => $this->getGalleryArray(),
		];
	}

	private function getSliderArray()
	{
		$output = [];
		$data = [];

		$columnName = MultiColumnVariable::select('name', 'id')->get();
		foreach ($columnName as $item) {
			$data[$item->id] = $item->name;
		}
		$contents = MultiVariableContent::select('content', 'multi_column_variable_id', 'multi_line_variable_id')->get();

		foreach ($contents as $item) {
			if (array_key_exists($item->multi_column_variable_id, $data)) {
				$output[$item->multi_line_variable_id][$data[$item->multi_column_variable_id]] = $item->content;
			}
		}
		return $output;
	}

	private function getGalleryArray()
	{
		// code...
	}
}
