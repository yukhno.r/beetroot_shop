<?php

use Illuminate\Database\Seeder;

class PaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Model\Payment::create([
        'name' => 'paypal',
        'description' => 'Paypal description',
        'link' => 'http://paypal.com'
      ]);
      App\Model\Payment::create([
        'name' => 'visa',
        'description' => 'Visa description',
        'link' => 'http://visa.com'
      ]);
      App\Model\Payment::create([
        'name' => 'mastercard',
        'description' => 'Mastercard description',
        'link' => 'http://mastercard.com'
      ]);
      App\Model\Payment::create([
        'name' => 'express',
        'description' => 'Express description',
        'link' => 'http://express.com'
      ]);
      App\Model\Payment::create([
        'name' => 'discover',
        'description' => 'Discover description',
        'link' => 'http://discover.com'
      ]);
    }
}
